import logging
import random

from wioframework.amodels import wideio_inject_extension


@wideio_inject_extension("accounts.UserAccount")
class GamifiedUserMixin:
    @staticmethod
    def get_suggested_action(self):
        # FIXME:  should this be made persistent and storin the db ?
        actions = self.get_potential_actions()
        if actions['required'] and len(actions['required']):
            return actions['required'][0]
        elif actions['important'] or actions['bonus']:
            if actions['important'] and actions['bonus']:
                actions = actions['important'] if random.randint(0, 2) else actions['bonus']
            else:
                actions = actions['important'] or actions['bonus']
            if len(actions):
                return random.choice(actions)
        return ["You have completed all suggested actions for now!", "/"]

    @staticmethod
    def get_nonessential_actions(self):
        actions = self.get_potential_actions()
        return actions['important'] + actions['bonus']

    @staticmethod
    def user_tasks(self, request, user, tasks):

        try:
            if not self.organisation:
                important.append(["Add an organisation", "/accounts/organisation/list/"])
        except:
            important.append(["Add an organisation", "/accounts/organisation/list/"])

    @staticmethod
    def get_potential_actions(self, request=None):
        """
        This function suggests different actions to the user as part of the gamification process
        """

        ## FIXME: THIS SHOULD BE MODULARISED
        ## FIXME: THIS FUNCTION SHOULD NOT MODIFY DB STATE !
        ## FIXME: WARN ABOUT EXPIRING DRAFTS

        tasks = {}
        tasks["required"] = []
        tasks["important"] = []
        tasks["bonus"] = []

        account_url = "/accounts/profile/%s"

        if self.date_of_birth:
            now = datetime.date.today()
            age = now - self.date_of_birth
            TD = age.days
            if ((TD < (365 * 3)) or (TD > (365 * 140))):
                tasks["required"].append(
                    ['Based on your date of birth, we cannot give you access to this website.', account_url % 'update'])

        teams = self.member_of.all()
        if not teams.count():
            tasks["bonus"].append(["Join a team", "/accounts/team/list/"])

        if self.is_staff:
            if not self.blogentry_rev_author.all():
                tasks["bonus"].append(['Write your first blog entry', '/direct/blog/'])
            elif self.blogentry_rev_author.filter(
                    updated_at__gt=datetime.date.today() - datetime.timedelta(28)).count():
                tasks["bonus"].append(['Update your blog', '/direct/blog/'])

        if self.is_scientist:
            tasks["important"].append(["Upload a new algorithm", "/science/algorithm/add/"])

        # For everyone ensure tag are being defined
        # actions.append(["Provide a few keywords about your interests",
        #            "/accounts/select_keywords/",
        #           10])
        # For everyone invite friends
        # act.append(["Invite new contacts to wide io",
        #            "/accounts/invite_someone_else/",
        #            self.followers.count()])

        actions = tasks
        if self._metadata is None:
            self._metadata = {}
        if 'actions' in self._metadata:
            old_actions = self._metadata['actions']

            def actions_sum(actions):
                return sum(map(lambda v: len(v), actions.values()))

            if actions_sum(old_actions) > actions_sum(actions):
                if request:
                    from django.contrib import messages
                    if len(old_actions['required']) and not len(actions['required']):
                        messages.add_message(request, messages.SUCCESS,
                                             'You have completed all your urgent actions. <a href="/" class="btn">Visit your dashboard</a>')
                    else:
                        # messages.add_message(request, messages.SUCCESS, "Congratulations! You completed ...")
                        pass
        self._metadata['actions'] = actions
        try:
            self.save(update_fields=["_metadata"])
        except:
            # 'CONVERSION_ERROR: error converting to timestamp from 2009-06-19 23:35:07.364146'
            logging.exception("[error during action processing...]")
        return actions


    # def gather_suggested_actions(self):
    #     """
    #     Looks for actions that the user needs to do on different 'channels'.
    #     Incomplete drafts, notifications to read, result of this may eventually be cached in the user record/hidden notifications...
    #     Some checks may be performed stochastically.
    #
    #     All gamified actions must have costs and benefits vectors associated with them.
    #     """
    #  Should be done by WIDEIO MODEL ON_REGISTER_MODEL

#def on_load_postprocess